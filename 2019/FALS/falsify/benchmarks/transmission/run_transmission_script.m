% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)

% ARCH2014 Benchmark
%%%%%%%%%%%%%%%%%%%%

tmpl = struct();
tmpl.mdl = 'Autotrans_shift';
tmpl.input_range = [0.0 100.0; 0.0 350.0];
tmpl.output_range = [0.0 160.0;0.0 5000.0;1.0 4.0];
tmpl.init_opts = {};
tmpl.gen_opts = {};
tmpl.interpolation = {'pconst'};
tmpl.option = {};
tmpl.maxEpisodes = maxEpisodes;
tmpl.agentName = 'Falsifier';

%Formula 1
fml1 = struct(tmpl);
fml1.expName = 'fml1';
fml1.targetFormula = '[]_[0,20]p1';
fml1.monitoringFormula = 'p1';
fml1.preds(1).str = 'p1';
fml1.preds(1).A = [1 0 0];
fml1.preds(1).b = 120.0;
fml1.stopTime = 20;

% Formula 2
fml2 = struct(tmpl);
fml2.expName = 'fml2';
fml2.targetFormula = '[]_[0,10]p1';
fml2.monitoringFormula = 'p1';
fml2.preds(1).str = 'p1';
fml2.preds(1).A = [0 1 0];
fml2.preds(1).b = 4750.0;
fml2.stopTime = 10;

%Formula 51
fml51 = struct(tmpl);
fml51.expName = 'fml51';
fml51.targetFormula = '[]_[0,30.0]( ((!g1L \/ !g1U) /\ <>_[0.001, 0.1] (!g1L \/ !g1U)) -> <>_[0.001,0.1][]_[0.0,2.5](g1L /\ g1U))';
fml51.monitoringFormula = '[.]_[260,260]((!g1L \/ !g1U) /\ <>_[0.1,10] (!g1L \/ !g1U)) -> <>_[0.1,10][]_[0.0,250](g1L /\ g1U)';
fml51.preds(1).str = 'g1L';
fml51.preds(1).A = [0 0 1];
fml51.preds(1).b = 1;
fml51.preds(2).str = 'g1U';
fml51.preds(2).A = [0 0 -1];
fml51.preds(2).b = -1;
fml51.stopTime = 30;

%Formula 52
fml52 = struct(tmpl);
fml52.expName = 'fml52';
fml52.targetFormula = '[]_[0,30.0]( ((!g2L \/ !g2U) /\ <>_[0.001, 0.1] (!g2L \/ !g2U)) -> <>_[0.001,0.1][]_[0.0,2.5](g2L /\ g2U))';
fml52.monitoringFormula = '[.]_[260,260]((!g2L \/ !g2U) /\ <>_[0.1, 10] (!g2L \/ !g2U)) -> <>_[0.1,10][]_[0.0,250](g2L /\ g2U)';
fml52.preds(1).str = 'g2L';
fml52.preds(1).A = [0 0 1];
fml52.preds(1).b = 2;
fml52.preds(2).str = 'g2U';
fml52.preds(2).A = [0 0 -1];
fml52.preds(2).b = -2;
fml52.stopTime = 30;

%Formula 53
fml53 = struct(tmpl);
fml53.expName = 'fml53';
fml53.targetFormula = '[]_[0,30.0]( ((!g3L \/ !g3U) /\ <>_[0.001, 0.1] (!g3L \/ !g3U)) -> <>_[0.001,0.1][]_[0.0,2.5](g3L /\ g3U))';
fml53.monitoringFormula = '[.]_[260,260]((!g3L \/ !g3U) /\ <>_[0.1, 10] (!g3L \/ !g3U)) -> <>_[0.1,10][]_[0.0,250](g3L /\ g3U)';
fml53.preds(1).str = 'g3L';
fml53.preds(1).A = [0 0 1];
fml53.preds(1).b = 3;
fml53.preds(2).str = 'g3U';
fml53.preds(2).A = [0 0 -1];
fml53.preds(2).b = -3;
fml53.stopTime = 30;

%Formula 54
fml54 = struct(tmpl);
fml54.expName = 'fml54';
fml54.targetFormula = '[]_[0,30.0]( ((!g4L \/ !g4U) /\ <>_[0.001, 0.1] (!g4L \/ !g4U)) -> <>_[0.001,0.1][]_[0.0,2.5](g4L /\ g4U))';
fml54.monitoringFormula = '[.]_[260,260]((!g4L \/ !g4U) /\ <>_[0.1,10] (!g4L \/ !g4U)) -> <>_[0.1,10][]_[0.0,250](g4L /\ g4U)';
fml54.preds(1).str = 'g4L';
fml54.preds(1).A = [0 0 1];
fml54.preds(1).b = 4;
fml54.preds(2).str = 'g4U';
fml54.preds(2).A = [0 0 -1];
fml54.preds(2).b = -4;
fml54.stopTime = 30;

formulas = {fml1, fml2, fml51, fml52,fml53, fml54};

configs = { };
for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.algoName = algorithms{i};
        config.sampleTime = 1;
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end

for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.algoName = algorithms{i};
        config.sampleTime = 5;
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end


do_experiment('transmission', configs);
