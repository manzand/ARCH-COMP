function hist_authors(directory)
% hist_authors - generates a histogram for each main file in the CORA
% directory. In total the function generates 11 histograms. The histograms
% show the authors that contributed to the corresponding file.
%
% Syntax:  
%    hist_authors(directory)
%
% Inputs:
%    directory: a string contains the full directory of the CORA files.
%
% Outputs:
%       no
%
% 
% Author:       Raja Judeh
% Written:      February-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%% Extracting the main files in the directory

files = dir(directory);
files = files([files.isdir]); %extracting the files that are directories
files(1:3) = []; %remove the first 3 unwanted files
files(8) = []; %remove the 'manual' directory since it contains no m. files
num_files = length(files); %number of the main directories (files)

%% Generating the histogram

for idx = 1:num_files
    % Extract the full path of the current main directory
    sub_directory = [files(idx).folder, '\', files(idx).name];
    
    %generate a histogram of the authors
    figure;
    histcounts_authors(sub_directory, true); 
    xlabel('Authors')
    ylabel('Number of Files')
    title(['Authors of', ' ', files(idx).name])
end

%------------- END OF CODE --------------

end


