#include "ibex.h"
#include "vibes.cpp"
#include "vibes.h"

using namespace ibex;

unsigned long int i = 0;
void toGunPlot (const simulation* sim, int a, int b, double time) {

  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    if (iterator_list->time_j.lb() > time)
      return;

    i++;
    std::cout << "set object " << i <<  " rect from " <<
      iterator_list->box_j1->operator[](a).lb() << ", " <<
      iterator_list->box_j1->operator[](b).lb() << " to " <<
      iterator_list->box_j1->operator[](a).ub() << ", " <<
      iterator_list->box_j1->operator[](b).ub() <<
      std::endl;
  }

}


void plot_simu(const simulation* sim, int a, int b, double time, string color)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  if (iterator_list->time_j.lb() > time)
      return;
      
    vibes::drawBox(iterator_list->box_j1->operator[](a).lb(), iterator_list->box_j1->operator[](a).ub(),iterator_list->box_j1->operator[](b).lb(), iterator_list->box_j1->operator[](b).ub(), color);
  }
}


Interval find_time(simulation &simu, int dimension, double borne)
{
  Interval temp;
  bool event=false;
  std::list<solution_g>::iterator iterator_list;
  for(iterator_list=simu.list_solution_g.begin();iterator_list!=simu.list_solution_g.end();iterator_list++)
    {
      //temp=iterator_list->time_j;
      if (iterator_list->box_jnh->operator[](dimension).ub() > borne)
	{

	  if (!event)
	    {
	      event = true;
	      temp = iterator_list->time_j;
	    }
	  else
	    temp|=iterator_list->time_j; //union with the last one
	}
      if (event && (iterator_list->box_jnh->operator[](dimension).lb() > borne))
	return temp;

    }
  return iterator_list->time_j;

}


int main(){


  const int ny= 4;
  Variable y(ny);
  IntervalVector yinit(ny);
  yinit[0] = Interval(-925,-875);
  yinit[1] = Interval(-425,-375);
  yinit[2] = Interval(0,5);//0);
  yinit[3] = Interval(0,5);//0);

  double mu = 3.986*(1e14)*60*60;
  double r = 42164*(1e3);
  double m=500;
  double n=sqrt(mu/pow(r,3));

  //mode approaching (y[0] < -100)
  double k11 = -28.8287;
  double k12 = 0.1005;
  double k13 = -1449.9754;
  double k14 = 0.0046;
  double k21 = -0.087;
  double k22 = -33.2562;
  double k23 = 0.00462;
  double k24 = -1451.5013;

  Function ydot = Function(y,Return(y[2],
				    y[3],
				    sqr(n)*y[0] + 2*n*y[3] + mu/sqr(r) - (mu/(sqr(r)*r))*(r+y[0])+(k11*y[0]+k12*y[1]+k13*y[2]+k14*y[3])/m,
				    sqr(n)*y[1] - 2*n*y[2] - (mu/(pow(sqr(r+y[0]) + sqr(y[1]),1.5)))*(y[1])+(k21*y[0]+k22*y[1]+k23*y[2]+k24*y[3])/m));


  ivp_ode problem = ivp_ode(ydot,0.0,yinit);

  simulation simu = simulation(&problem,120.0,KUTTA3,1e-6);

  simu.run_simulation();

  //search of event
  double L=-100;
  Interval instant = find_time(simu, 0, L);
  //  std::cout << instant << std::endl;
  IntervalVector middle = simu.get(instant);

  /*std::cout << "set terminal png" << std::endl;
  std::cout << "set output 'spacecraft.png'" << std::endl;
  std::cout << "set xrange [-1000:200]" << std::endl;
  std::cout << "set yrange [-450:0]" << std::endl;
  std::cout << "set nokey" << std::endl;
  std::cout << "set style rectangle back fc rgb 'red' fs solid 1.0 noborder" << std::endl;
  toGunPlot (&simu, 0, 1, instant.ub());
*/
  vibes::beginDrawing ();
  vibes::newFigure("spacecraft");
  plot_simu(&simu, 0, 1, instant.ub(), "blue[blue]");

  //mode rdv (y[0] >= -100)

  k11 = -288.0288;
  k12 = 0.1312;
  k13 = -9614.9898;
  k14 = 0;
  k21 = -0.1312;
  k22 = -288;
  k23 = 0;
  k24 = -9614.9883;

  Function ydot2 = Function(y,Return(y[2],
				     y[3],
				     sqr(n)*y[0] + 2*n*y[3] + mu/sqr(r) - (mu/(sqr(r)*r))*(r+y[0])+(k11*y[0]+k12*y[1]+k13*y[2]+k14*y[3])/m,
				     sqr(n)*y[1] - 2*n*y[2] - (mu/(pow(sqr(r+y[0]) + sqr(y[1]),1.5)))*(y[1])+(k21*y[0]+k22*y[1]+k23*y[2]+k24*y[3])/m));


  ivp_ode problem2 = ivp_ode(ydot2,instant.lb(),middle);

  simulation simu2 = simulation(&problem2,120.0,KUTTA3,1e-6);

  simu2.run_simulation();



  //std::cout << "set style rectangle back fc rgb 'orange' fs solid 1.0 noborder" << std::endl;
  //toGunPlot (&simu2, 0, 1, 120.0);
plot_simu(&simu2, 0, 1, 120.0, "red[red]");
  //mode aborting u=0

  k11 = 0;
  k12 = 0;
  k13 = 0;
  k14 = 0;
  k21 = 0;
  k22 = 0;
  k23 = 0;
  k24 = 0;

  Function ydot3 = Function(y,Return(y[2],
				     y[3],
				     sqr(n)*y[0] + 2*n*y[3] + mu/sqr(r) - (mu/(sqr(r)*r))*(r+y[0]),
				     sqr(n)*y[1] - 2*n*y[2] - (mu/(pow(sqr(r+y[0]) + sqr(y[1]),1.5)))*(y[1])));


  ivp_ode problem3 = ivp_ode(ydot3,120.0,simu2.get_last());

  simulation simu3 = simulation(&problem3,200.0,KUTTA3,1e-6);

  simu3.run_simulation();
  //std::cout << "set style rectangle back fc rgb 'blue' fs solid 1.0 noborder" << std::endl;
  //toGunPlot (&simu3, 0, 1, 200.0);
  plot_simu(&simu3, 0, 1, 200, "green[green]");
  std::cout << "Final volume : " << (simu3.get_last()).volume() << std::endl;
  
     vibes::axisLimits(-1000,200,-450,0);
  vibes::setFigureProperty("spacecraft","width",800);//1920);
  vibes::setFigureProperty("spacecraft","height",600);//1080);
    vibes::saveImage("spacecraft.jpg","spacecraft");
    vibes::closeFigure("spacecraft");
  vibes::endDrawing();

  return 0;

}
