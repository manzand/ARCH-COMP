from src.actors.envs.environment import AbstractEnvironment
from src.network_parser.network_model import NetworkModel
from src.verification.bounds.bounds import HyperRectangleBounds


class AccEnv(AbstractEnvironment):

    def __init__(self, square_model):
        """
        Environment for the Adaptive Cruise Control benchmark
        """

        assert isinstance(square_model, NetworkModel)

        # set the branching factor to 1
        super(AccEnv, self).__init__(1)

        self.square_model = square_model

        self.dt = 0.1
        self.a_lead = -2.0
        self.T_gap = 1.4
        self.D_default = 10
        self.mu = 0.0001

    def get_constraints_for_transition(self, i, constrs_manager, action_vars, input_state_vars):
        """
        :param i:
        :param constrs_manager:
        :param action_vars:
        :param input_state_vars: variables representing the current state
        :return:
        """

        if len(input_state_vars) != 6:
            raise Exception("Expecting exactly 6 variables representing an extended observation of environment state "
                            "(x, v and gamma for lead and ego vehicles. "
                            "Instead got {} variables".format(len(input_state_vars)))

        if len(action_vars) != 1:
            raise Exception("Expecting exactly one variable representing an agent's action. "
                            "Instead got {} variables".format(len(action_vars)))

        constrs = []

        [x1, x2, x3, x4, x5, x6] = input_state_vars
        a_ego = action_vars[0]

        # Get and add the constraint for computing square via NN aproximation.
        [x2_square], x2_square_constrs = constrs_manager.get_network_constraints(self.square_model.layers, [x2])
        constrs.extend(x2_square_constrs)

        [x4_square], x4_square_constrs = constrs_manager.get_network_constraints(self.square_model.layers, [x4])
        constrs.extend(x4_square_constrs)



        # Compute bounds for next state variables
        bounds = constrs_manager.get_variable_bounds([x1, x2, x3, x4, x5, x6, a_ego])
        x2_square_bounds = constrs_manager.get_variable_bounds([x2_square])
        x4_square_bounds = constrs_manager.get_variable_bounds([x4_square])



        next_x1_lower, next_x1_upper = self.next_x1_bounds(bounds.get_lower(), bounds.get_upper())
        next_x2_lower, next_x2_upper = self.next_x1_bounds(bounds.get_lower(), bounds.get_upper())

        next_x3_lower, next_x3_upper = \
            self.next_x3_bounds(bounds.get_lower() + x2_square_bounds.get_lower(),
                                       bounds.get_upper() + x2_square_bounds.get_upper())

        next_x4_lower, next_x4_upper = self.next_x4_bounds(bounds.get_lower(), bounds.get_upper())
        next_x5_lower, next_x5_upper = self.next_x5_bounds(bounds.get_lower(), bounds.get_upper())

        next_x6_lower, next_x6_upper = \
            self.next_x6_bounds(bounds.get_lower() + x4_square_bounds.get_lower(),
                                       bounds.get_upper() + x4_square_bounds.get_upper())

        # Create next state variables with computed bounds
        [next_x1, next_x2,next_x3,next_x4,next_x5,next_x6] = \
            constrs_manager.create_state_variables(6,
                                                   lbs=[next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower,next_x5_lower,next_x6_lower],
                                                   ubs=[next_x1_upper,next_x2_upper, next_x3_upper, next_x4_upper, next_x5_upper, next_x6_upper])
        constrs_manager.add_variable_bounds([next_x1, next_x2,next_x3,next_x4,next_x5,next_x6],
                                            HyperRectangleBounds([next_x1_lower, next_x2_lower, next_x3_lower, next_x4_lower,next_x5_lower,next_x6_lower],
                                                                 [next_x1_upper,next_x2_upper, next_x3_upper, next_x4_upper, next_x5_upper, next_x6_upper]))


        # Add the constraints for computing next_x1 and others

        # next_x1 == x1 + x2 * self.dt
        # x1(t+1) = x1(t) + x2(t) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_x1, x1, x2],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x2 == x2 + x3 * self.dt
        # x2(t+1) = x2(t) + x3(t) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_x2, x2, x3],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x3 == x3  - 2 * x3 * self.dt + 2 * self.a_lead - self.mu * x2_square * self.dt

        constrs.append(
            constrs_manager.get_linear_constraint([next_x3, x3, x2_square],
                                                  [-1, (1 - 2 * self.dt), (-1 * self.mu * self.dt)],
                                                  (-2 * self.a_lead)))

        # next_x4 == x4 + x5 * self.dt
        # x4(t+1) = x4(t) + x5(t) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_x4, x4, x5],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x5 == x5 + x6 * self.dt
        # x5(t+1) = x5(t) + x6(t) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_x5, x5, x6],
                                                             [-1, 1, self.dt],
                                                             0))

        # next_x6 == x6  - 2 * x6 * self.dt + 2 * a_ego - self.mu * x4_square * self.dt

        constrs.append(
            constrs_manager.get_linear_constraint([next_x6, x6, a_ego, x4_square],
                                                  [-1, (1 - 2 * self.dt), 2 , (-1 * self.mu * self.dt)],
                                                  0))

        return [next_x1, next_x2,next_x3,next_x4,next_x5,next_x6], constrs

    ########
    ## in the following I am assuming variables are renamed following the
    ## dynamicsACC.m file, i.e.,

    # % x1 = lead_car position
    # % x2 = lead_car velocity
    # % x3 = lead_car internal state (gamma)
    #
    # % x4 = ego_car position
    # % x5 = ego_car velocity
    # % x6 = ego_car internal state (gamma)

    # x2_square
    # x4_square

    ## I assume variables appear in bound vector in this order.
    ########

    def next_x1_bounds(self, input_lower, input_upper):
        """
        """
        # x1(t+1) = x1(t) + x2(t) * dt
        upper = input_upper[0] + self.dt * input_upper[1]
        lower = input_lower[0] + self.dt * input_lower[1]
        return lower, upper

    def next_x2_bounds(self, input_lower, input_upper):
        """
        """
        # x2(t+1) = x2(t) + x3(t) * dt
        upper = input_upper[1] + self.dt * input_upper[2]
        lower = input_lower[1] + self.dt * input_lower[2]
        return lower, upper

    def next_x3_bounds(self, input_lower, input_upper):
        """
        """
        # x3(t+1) = x3(t) - 2 * x3(t) * dt + 2 * self.a_lead - self.mu * x2(t) * x2(t) * dt

        upper = input_upper[2] - 2 * self.dt * input_upper[2] + 2 * self.a_lead - self.mu * input_upper[-1] * self.dt
        lower = input_lower[2] - 2 * self.dt * input_lower[2] + 2 * self.a_lead - self.mu * input_lower[-1] * self.dt
        return lower, upper

    def next_x4_bounds(self, input_lower, input_upper):
        """
        """
        # x4(t+1) = x4(t) + x5(t) * dt
        upper = input_upper[3] + self.dt * input_upper[4]
        lower = input_lower[3] + self.dt * input_lower[4]
        return lower, upper

    def next_x5_bounds(self, input_lower, input_upper):
        """
        """
        # x5(t+1) = x5(t) + x6(t) * dt
        upper = input_upper[4] + self.dt * input_upper[5]
        lower = input_lower[4] + self.dt * input_lower[5]
        return lower, upper

    def next_x6_bounds(self, input_lower, input_upper):
        """
        """
        # x6(t+1) = x6(t) - 2 * x6(t) * dt + 2 * self.a_lead - self.mu * x4(t) * x4(t) * dt
        
        upper = input_upper[5] - 2 * self.dt * input_upper[5] + 2 * self.a_lead - self.mu * input_upper[-1] * self.dt
        lower = input_lower[5] - 2 * self.dt * input_lower[5] + 2 * self.a_lead - self.mu * input_lower[-1] * self.dt
        return lower, upper
