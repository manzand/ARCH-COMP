function [Tf,ind] = thirdOrderTensor_springpendulum(x,u)



 Tf{1,1} = interval(sparse(5,5),sparse(5,5));



 Tf{1,2} = interval(sparse(5,5),sparse(5,5));



 Tf{1,3} = interval(sparse(5,5),sparse(5,5));



 Tf{1,4} = interval(sparse(5,5),sparse(5,5));



 Tf{1,5} = interval(sparse(5,5),sparse(5,5));



 Tf{2,1} = interval(sparse(5,5),sparse(5,5));



 Tf{2,2} = interval(sparse(5,5),sparse(5,5));



 Tf{2,3} = interval(sparse(5,5),sparse(5,5));



 Tf{2,4} = interval(sparse(5,5),sparse(5,5));



 Tf{2,5} = interval(sparse(5,5),sparse(5,5));



 Tf{3,1} = interval(sparse(5,5),sparse(5,5));

Tf{3,1}(4,4) = 2;


 Tf{3,2} = interval(sparse(5,5),sparse(5,5));

Tf{3,2}(2,2) = (981*sin(x(2)))/100;


 Tf{3,3} = interval(sparse(5,5),sparse(5,5));



 Tf{3,4} = interval(sparse(5,5),sparse(5,5));

Tf{3,4}(4,1) = 2;
Tf{3,4}(1,4) = 2;


 Tf{3,5} = interval(sparse(5,5),sparse(5,5));



 Tf{4,1} = interval(sparse(5,5),sparse(5,5));

Tf{4,1}(1,1) = (6*((981*sin(x(2)))/100 + 2*x(3)*x(4)))/x(1)^4;
Tf{4,1}(2,1) = -(981*cos(x(2)))/(50*x(1)^3);
Tf{4,1}(3,1) = -(4*x(4))/x(1)^3;
Tf{4,1}(4,1) = -(4*x(3))/x(1)^3;
Tf{4,1}(1,2) = -(981*cos(x(2)))/(50*x(1)^3);
Tf{4,1}(2,2) = -(981*sin(x(2)))/(100*x(1)^2);
Tf{4,1}(1,3) = -(4*x(4))/x(1)^3;
Tf{4,1}(4,3) = 2/x(1)^2;
Tf{4,1}(1,4) = -(4*x(3))/x(1)^3;
Tf{4,1}(3,4) = 2/x(1)^2;


 Tf{4,2} = interval(sparse(5,5),sparse(5,5));

Tf{4,2}(1,1) = -(981*cos(x(2)))/(50*x(1)^3);
Tf{4,2}(2,1) = -(981*sin(x(2)))/(100*x(1)^2);
Tf{4,2}(1,2) = -(981*sin(x(2)))/(100*x(1)^2);
Tf{4,2}(2,2) = (981*cos(x(2)))/(100*x(1));


 Tf{4,3} = interval(sparse(5,5),sparse(5,5));

Tf{4,3}(1,1) = -(4*x(4))/x(1)^3;
Tf{4,3}(4,1) = 2/x(1)^2;
Tf{4,3}(1,4) = 2/x(1)^2;


 Tf{4,4} = interval(sparse(5,5),sparse(5,5));

Tf{4,4}(1,1) = -(4*x(3))/x(1)^3;
Tf{4,4}(3,1) = 2/x(1)^2;
Tf{4,4}(1,3) = 2/x(1)^2;


 Tf{4,5} = interval(sparse(5,5),sparse(5,5));


 ind = cell(4,1);
 ind{1} = [];


 ind{2} = [];


 ind{3} = [1;2;4];


 ind{4} = [1;2;3;4];

