function runTestSuite_nn(varargin)
% runTestSuite_nn - runs the standard test suite by executing all functions
%    starting with the prefix 'test_nn'
%
% Syntax:  
%    runTestSuite_nn(varargin)
%
% Inputs:
%    verbose - show workspace output or not (not required)
%    directory - change directory (not required)
%
% Outputs:
%    -

% Author:       Tobias Ladner
% Written:      03-March-2022
% Last update:  28-November-2022 (testnn)
% Last revision:---

%------------- BEGIN CODE --------------

% too many input arguments
if nargin > 2
    throw(CORAerror('CORA:tooManyInputArgs',2));
end

% set default values
[directory,verbose] = setDefaultValues({[CORAROOT filesep 'unitTests'],true},varargin);


% run main program performing the tests
prefixes = {
    % all 'neuralNetwork' nn tests without additional toolboxes
    'test_nn';
    % all tests requiring additional toolboxes
    'testnn';
};
failed = {};
numberOfTests = 0;

% run suite
for i=1:length(prefixes)
    prefix = prefixes{i};
    [failed_, numberOfTests_] = testSuiteCore(prefix,verbose,directory);
    failed = [failed; failed_];
    numberOfTests = numberOfTests + numberOfTests_;
end

% run additional tests
docstringtest = 'test_docstring';
fprintf("run %s:", docstringtest)
[~,res] = evalc(docstringtest);
if res
    fprintf(" passed\n")
else
    fprintf(" failed\n")
    failed = [failed, {docstringtest}];
end
numberOfTests = numberOfTests + 1;

disp('----------------------------------------------------------------------------');
disp(['run ' int2str(numberOfTests) ' tests, ' int2str(length(failed)) ' failed.']);
disp(strjoin(failed, ',\n'));

%------------- END OF CODE --------------