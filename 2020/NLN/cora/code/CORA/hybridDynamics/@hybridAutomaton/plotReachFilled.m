function han = plotReachFilled(obj,varargin)
% plotReachFilled - plots reachable sets of a hybrid automaton
%
% Syntax:  
%    plotReachFilled(obj)
%    plotReachFilled(obj,dim)
%    plotReachFilled(obj,dim,color)
%    plotReachFilled(obj,dim,color,order)
%
% Inputs:
%    obj - hybrid automaton object
%    dim - dimension to be plottet
%    color - color of the plot (cell if different for each location)
%    order - zonotope order of the plotted zonotopes
%
% Outputs:
%    han - handle to the plotted objects
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author: Matthias Althoff
% Written: 11-May-2007 
% Last update: 28-September-2007
%              26-March-2008
%              23-June-2009
%              10-November-2010
%              23-November-2017
% Last revision: ---

%------------- BEGIN CODE --------------

% default values
dim = [1,2];
color = 'b';
order = [];

if nargin >= 2 && ~isempty(varargin{1})
   dim = varargin{1}; 
end
if nargin >= 3 && ~isempty(varargin{2})
   color = varargin{2}; 
end
if nargin >= 4 && ~isempty(varargin{3})
   order = varargin{3}; 
end

% load data from object structure
R = obj.result.reachSet.Rcont;
loc = obj.result.reachSet.location;

han = cell(max(loc),1);

% loop over the reachable sets for all locations
for i=1:(length(R))

    % loop over the reachable sets for all time points
    for j=1:length(R{i}.OT)
        
        
        if ~iscell(R{i}.OT{j}) % no split sets

            Rtemp = R{i}.OT{j};
            dimTemp = dim;
            
            % enclose polynomial zonotopes
            if isa(Rtemp,'polyZonotope')
               Rtemp = zonotope(Rtemp); 
            end
            
            % reduce zonotope order
            if (isa(Rtemp,'zonotope') || isa(Rtemp,'zonoBundle')) && ...
                ~isempty(order)
            
                Rtemp = project(Rtemp,dim);
                Rtemp = reduce(Rtemp,'girard',order);
                dimTemp = [1,2];
            end
            
            % plot reachable set
            if iscell(color)
                han{loc(i)} = plotFilled(Rtemp,dimTemp,color{loc(i)}, ...
                                         'EdgeColor','none');
            else
                han{loc(i)} = plotFilled(Rtemp,dimTemp,color,'EdgeColor','none');
            end
            
        else % sets are split
            
            for iSubSet=1:length(R{i}.OT{j})

                Rtemp = R{i}.OT{j}{iSubSet};
                dimTemp = dim;
                
                % enclose polynomial zonotopes
                if isa(Rtemp,'polyZonotope')
                   Rtemp = zonotope(Rtemp); 
                end
                
                % redcue zonotope order
                if (isa(Rtemp,'zonotope') || isa(Rtemp,'zonoBundle')) && ...
                   ~isempty(order)
               
                    Rtemp = project(Rtemp,dim);
                    Rtemp = reduce(Rtemp,'girard',options.polytopeOrder);
                    dimTemp = [1,2];
                end
                
                % plot reachable set
                if iscell(color)
                    han{loc(i)} = plotFilled(Rtemp,dimTemp,color{loc(i)}, ...
                                             'EdgeColor','none');
                else
                    han{loc(i)} = plotFilled(Rtemp,dimTemp,color, ...
                                             'EdgeColor','none');
                end
            end
        end
    end
end

% get handle to plotted objects
if ~iscell(color)
   han = han{loc(1)}; 
end


%------------- END OF CODE --------------