'''
This is the International Space Station example used in the ARCH reachability tools competition in 2018.

It is a 271-dimensional continuous system, with three time-varying inputs.

This model demonstrates:
- Loading dynamics from matlab .mat files
- Error condition that is a disjunction
- Plotting with a linear combination of the system states
- Plotting over time without introducing a new variable
'''

import sys
import time
from matplotlib import collections

from scipy.io import loadmat
import numpy as np

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil, lpinstance

def make_automaton(limit=0.0005, spec=1):
    'make the hybrid automaton'

    ha = HybridAutomaton()

    mode = ha.new_mode('mode')
    dynamics = loadmat('iss.mat')
    a_matrix = dynamics['A']
    b_matrix = dynamics['B']
    n = a_matrix.shape[1]

    # add the input as a state variable
    big_a_mat = np.zeros((n+3, n+3))
    big_a_mat[0:n, 0:n] = a_matrix.toarray()
    big_a_mat[0:n, n:n+3] = b_matrix.toarray()

    mode.set_dynamics(big_a_mat)

    # the third output defines the unsafe condition
    y3 = dynamics['C'][2].toarray()
    big_y3 = np.zeros((n + 3,), dtype=float)
    big_y3[:n] = y3

    error = ha.new_mode('error')

    # Error condition: y3 * x <= -limit OR y3 >= limit
    if spec == 1:
        trans = ha.new_transition(mode, error)
        trans.set_guard(big_y3, [-limit])
        
    if spec == 2:
        trans = ha.new_transition(mode, error)
        trans.set_guard(-1 * big_y3, [-limit])

    return ha

def make_init(ha):
    'make the initial states'

    # initial set has every variable as [-0.0001, 0.0001]
    mode = ha.modes['mode']

    dims = mode.a_csr.shape[0]
    init_box = (dims - 3) * [[-0.0001, 0.0001]]

    # bounds on the three constant inputs
    # 0 <= u1 <= 0.1
    # 0.8 <= u2 <= 1.0
    # 0.9 <= u3 <= 1.0

    init_box.append([0, 0.1])
    init_box.append([0.8, 1.0])
    init_box.append([0.9, 1.0])
    
    init_lpi = lputil.from_box(init_box, mode)
    
    init_list = [StateSet(init_lpi, mode)]

    return init_list

def make_settings(lines=None, filename=None):
    'make the reachability settings object'

    # see hylaa.settings for a list of reachability settings
    settings = HylaaSettings(0.01, 20.0)
    settings.plot.plot_mode = PlotSettings.PLOT_NONE
    settings.stdout = HylaaSettings.STDOUT_VERBOSE

    settings.plot.xdim_dir = None # x dimension will be time

    dynamics = loadmat('iss.mat')
    y3 = dynamics['C'][2]
    n = y3.shape[1]
    big_y3 = np.zeros((n + 3,), dtype=float)
    big_y3[:n] = y3.toarray()
    settings.plot.ydim_dir = big_y3 # use y3 for the y plot direction

    if filename is not None:
        settings.plot.filename = filename
        settings.plot.plot_mode = PlotSettings.PLOT_IMAGE
        settings.plot.label.title = ''
        settings.plot.label.y_label = '$y_{3}$'
        settings.plot.label.x_label = '$t$'
        settings.plot.plot_size = (6, 6)
        settings.plot.label.axes_limits = (0, 20, -0.0002, 0.0002)

        # add lines
        line_pts = []
        
        for line in lines:
            tmax = settings.num_steps * settings.step_size
            
            line_pts.append([(0, line), (tmax, line)])
            
        c1 = collections.LineCollection(line_pts, animated=True, colors=('red'), linewidths=(2), linestyle='dashed')
        
        settings.plot.extra_collections = [[c1]]

    return settings

def run_hylaa():
    'main entry point'

    safe_limit = 0.0005
    unsafe_limit = 0.00017
    
    if len(sys.argv) > 1 and sys.argv[1] == 'safe':
        # it's more efficient to check one spec at a time due to warm-start LP
        start = time.time()
        for spec in [1, 2]:
            print(f"Checking safe spec {spec}")
            ha = make_automaton(limit=safe_limit, spec=spec)
            init_states = make_init(ha)
            settings = make_settings()

            res = Core(ha, settings).run(init_states)
            assert not res.has_concrete_error

        time_safe = time.time() - start

        print(f"Safe Result:\nISSC01_S01 - {time_safe:.2f}")
    elif len(sys.argv) > 1 and sys.argv[1] == 'unsafe':
        # check unsafe case
        start = time.time()
        for spec in [1, 2]:
            print(f"Checking unsafe spec {spec}")
            ha = make_automaton(limit=unsafe_limit, spec=spec)
            init_states = make_init(ha)
            settings = make_settings()

            res = Core(ha, settings).run(init_states)

            if res.has_concrete_error:
                break

        assert res.has_concrete_error, "expected error but didn't find it"

        time_unsafe = time.time() - start
        print(f"Unsafe Result:\nISSC01_U01 - {time_unsafe:.2f}")
    elif len(sys.argv) > 1 and sys.argv[1] == 'plot':
        ha = make_automaton(limit=safe_limit, spec=None)
        init_states = make_init(ha)
        lines = [safe_limit, unsafe_limit, -unsafe_limit, -safe_limit]
        settings = make_settings(lines, filename='ISSC01.png')

        res = Core(ha, settings).run(init_states)
    else:
        print("expected single argument: safe/unsafe/plot")

    return not res.has_concrete_error

def save_csv():
    'run and save to csv'

    start = time.perf_counter()
    is_safe = run_hylaa()
    secs = time.perf_counter() - start

    if sys.argv[1] != 'plot':
        verified = 1 if is_safe else -1
        instance = "ISS02" if sys.argv[1] == 'safe' else 'ISSU02'

        with open("../results.csv", "a") as f:
            f.write(f"Hylaa,ISSC01,{instance},{verified},{secs},\n")

if __name__ == "__main__":
    save_csv()
