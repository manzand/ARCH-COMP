function [center, Gunred, Gred] = pickedGenerators(Z,order)
% pickedGenerators - Selects generators to be reduced
%
% Syntax:  
%    [center, Gunred, Gred] = pickedGenerators(Z,order)
%
% Inputs:
%    Z - zonotope object
%    order - desired order of the zonotope
%
% Outputs:
%    center - center of reduced zonotope
%    Gunred - generators that are not reduced
%    Gred - generators that are reduced
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%

% Author:       Matthias Althoff
% Written:      11-October-2017 
% Last update:  28-October-2017
%               14-March-2019 (vector norm exchanged, remove sort)
% Last revision:---

%------------- BEGIN CODE --------------

%get Z-matrix from zonotope Z
Zmatrix = get(Z,'Z');

%center
center = Zmatrix(:,1);

%extract generator matrix
G = Zmatrix(:,2:end);

%number of generators
nrOfGens = length(G(1,:));

%default values
Gunred = [];
Gred = [];

if ~isempty(G)

    %determine dimension of zonotope
    dim = length(G(:,1));
    
    %only reduce if zonotope order is greater than the desired order
    if length(G(1,:))>dim*order

        %compute metric of generators
        h = vecnorm(G,1) - vecnorm(G,inf);

        %number of generators that are not reduced
        nUnreduced = floor(dim*(order-1));
        %number of generators that are reduced
        nReduced = nrOfGens - nUnreduced;

        %pick generators with smallest h values to be reduced
        [~,ind] = mink(h,nReduced);
        Gred = G(:,ind);
        %unreduced generators
        indRemain = setdiff(1:nrOfGens, ind);
        Gunred = G(:,indRemain);
    else
        Gunred = G;
    end
end


%------------- END OF CODE --------------