function [obj,Rcont,loc] = reach(obj,params,options)
% reach - computes the reachable set of a hybrid automaton
%
% Syntax:  
%    [obj,Rcont,loc] = reach(obj,params,options)
%
% Inputs:
%    obj - hybrid automaton object
%    params - parameter defining the reachability problem
%    options - options for the computation of the reachable set
%
% Outputs:
%    obj - hybrid automaton object
%    Rcont - list of reachable sets
%    loc - list of visited locations
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: location/reach

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      07-May-2007 
% Last update:  16-August-2007
%               26-March-2008
%               07-October-2008
%               21-April-2009
%               20-August-2013
%               30-October-2015
%               22-August-2016
%               19-December-2019 (NK, restructured the algorithm)
% Last revision: ---

%------------- BEGIN CODE --------------

% check user settings
options = params2options(params,options);
options = checkOptionsReach(obj,options);

% initialization
Rcont = {};
loc = [];

list{1}.set = options.R0;
list{1}.loc = options.startLoc;
list{1}.tMin = options.tStart;
list{1}.tMax = options.tStart;

% loop until the queue is empty
while ~isempty(list)

    % get first element of the queue
    locID = list{1}.loc;
    R0 = list{1}.set;
    tStart = list{1}.tMin;

    list = list(2:end);

    % get input and time step for the current location
    options.U = options.Uloc{locID};
    options.timeStep = options.timeStepLoc{locID};

    % compute the reachable set within a location
    [R,Rjump] = reach(obj.location{locID},R0,tStart,options);

    % add the new sets to the queue
    list = [list,Rjump];

    % store the computed reachable set
    Rcont = [Rcont,R];
    loc = [loc,locID];       
end

% save results to object structure
obj.result.reachSet.location = loc;
obj.result.reachSet.Rcont = Rcont;
    

end

%------------- END OF CODE --------------