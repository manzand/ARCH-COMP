function obj = readYMLNetwork(file_path)
% readYMLNetwork - reads and converts a network saved in yml format
%
% Syntax:
%    res = NeuralNetwork.readYMLNetwork(file_path)
%
% Inputs:
%    file_path: path to file
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralnetwork2cora

% Author:       Tobias Ladner
% Written:      30-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% TODO: remove dependency on old neuralNetwork

% first convert to old neuralNetwork
old_nn = neuralnetwork2cora(file_path);

% then convert to new NeuralNetwork
obj = NeuralNetwork.getFromOldNeuralNetwork(old_nn);

end