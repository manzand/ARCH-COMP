%
%   plot_domain.m
%
%   created on: 01.01.2020
%       author: kaushik
%
%   this program plots the over and under-approximation of winning region
%
%   you need to run ./vehicle binary first
%   so that the controller bdds are generated
%   

% path of the SymbolicSet.m and the associated mex-file
addpath(genpath('../../../../'));

% load the controllers
co = SymbolicSet('vehicle_winning_over.bdd');
cu = SymbolicSet('vehicle_controller_under.bdd');
cw = SymbolicSet('vehicle_controller_wc.bdd');

% state space bounds 
lb = [0.0;0.0;-pi-0.5];
ub = [2.0;3.0;pi+0.5];

% prepare the figure window
figure;
axis([lb(1) ub(1) lb(2) ub(2) lb(3) ub(3)]);
view(-45,15);
hold on;

% plot the over-approximation
try
    po = co.points;
    scatter3(po(:,1),po(:,2),po(:,3),'.','MarkerFaceColor',[0.7 0.7 0.7],'MarkerEdgeColor',[0.7 0.7 0.7]);
catch
    warning('Points could not be loaded from controller_over.bdd\n');
end

% plot the under-approximation
try
    pu = cu.points;
    scatter3(pu(:,1),pu(:,2),pu(:,3),'.','MarkerFaceColor',[0 0.4470 0.7410],'MarkerEdgeColor',[0 0.4470 0.7410]);
catch
    warning('Points could no be loaded from controller_under.bdd\n');
end

% plot the worst case controller domain
try
    pw = cw.points;
    scatter3(pw(:,1),pw(:,2),pw(:,3),'.','MarkerFaceColor','r','MarkerEdgeColor','r');
catch
    warning('Points could no be loaded from controller_wc.bdd\n');
end
savefig('vehicle_domain');