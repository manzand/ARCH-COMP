function example_nonlinear_reach_ARCH19_vanDerPol()
% example_nonlinear_reach_ARCH19_vanDerPol - example of
% nonlinear reachability analysis. The guard intersections for the
% pseudoinvariant are calculated with Girards method
%
% Syntax:  
%    example_nonlinear_reach_ARCH19_vanDerPol()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      02-April-2017
% Last update:  13-March-2018
% Last revision:---


%------------- BEGIN CODE --------------


% OPTIONS -----------------------------------------------------------------
options.tStart=0; % start time
options.x0 = [1.4; 2.3]; % initial state for simulation

% initial set
R01 = zonotope(interval([1.25;2.35],[1.55;2.45]));
R02 = zonotope(interval([1.55;2.35],[1.85;2.45]));

options.startLoc = 1; %initial location
options.finalLoc = 0; %2: gears are meshed
options.taylorTerms=3; %number of taylor terms for reachable sets
options.zonotopeOrder=20; %zonotope order
options.polytopeOrder=4; %polytope order

options.advancedLinErrorComp = 0;
options.tensorOrder=2;
options.reductionTechnique='girard';
options.isHyperplaneMap=0;
options.guardIntersect = 'zonoGirard';
options.enclosureEnables = [1];

options.filterLength = [10, 5];
options.maxError=0.5*[1; 1];
options.reductionInterval=inf;
options.verbose = 1;

%uncertain inputs
options.uTrans = 0;
options.U = zonotope(0); %input for reachability analysis


% SYSTEM DYNAMICS ---------------------------------------------------------

vanderPol1 = nonlinearSys(2,1,@vanderPolEq,options);
vanderPol2 = nonlinearSys(2,1,@vanderPolEqMu2,options);



% HYBRID AUTOMATON --------------------------------------------------------

% define large and small distance
dist = 1e3;
smallDist = 1e3*eps;

% first guard set
guard1 = constrainedHyperplane(halfspace([1, 0],1.5),[0 1;0 -1],[0;2]);
guard22 = constrainedHyperplane(halfspace([1, 0],1.8),[0 1;0 -1],[0;2]);
guard23 = constrainedHyperplane(halfspace([-1,0],1.8),[0 1;0 -1],[1,0]);

% resets
reset.A = eye(2); 
reset.b = zeros(2,1);

% transition
trans1{1} = transition(guard1,reset,2,'a','b');
trans22{1} = transition(guard22,reset,2,'a','b');
trans23{1} = transition(guard23,reset,3,'a','b');

% invariant
inv1 = interval([1.5-smallDist; -dist],[dist;dist]);
inv22 = interval([1.8-smallDist; -dist],[dist;dist]);
inv23 = interval([-dist;-dist],[dist;0.5]);

% specify location
loc1{1} = location('loc1',1,inv1,trans1,vanderPol1); 
loc1{2} = location('loc2',2,ones(2,1)*interval(-dist,dist),[],vanderPol1); 

loc2{1} = location('loc1',1,inv22,trans22,vanderPol2); 
loc2{2} = location('loc2',2,inv23,trans23,vanderPol2); 
loc2{3} = location('loc3',3,ones(2,1)*interval(-dist,dist),[],vanderPol2);

% specify hybrid automata
HA1 = hybridAutomaton(loc1);
HA2 = hybridAutomaton(loc2);


% time step
options1 = options;
options2 = options;

options1.timeStepLoc{1}=0.01; 
options1.timeStepLoc{2}=0.01; 

options2.timeStepLoc{1}=0.01;
options2.timeStepLoc{2}=0.01;
options2.timeStepLoc{3}=0.01;

% set input:
for i=1:2
    options1.uLoc{i} = 0;
    options1.uLocTrans{i} = options1.uLoc{i};
    options1.Uloc{i} = zonotope(0);
end

for i=1:3
    options2.uLoc{i} = 0;
    options2.uLocTrans{i} = options2.uLoc{i};
    options2.Uloc{i} = zonotope(0);
end




% SIMULATION --------------------------------------------------------------

%obtain random simulation results
options1.R0 = R01;
options1.tFinal = 7;

for i=1:10
    %set initial state, input
    if i <=4 
        options1.x0 = randPointExtreme(options1.R0); %initial state for simulation
    else
        options1.x0 = randPoint(options1.R0); %initial state for simulation
    end 
    
    %simulate hybrid automaton
    HAsim = simulate(HA1,options1); 
    simRes{i} = get(HAsim,'trajectory');
end

HA1 = simulate(HA1,options1); 

 

% REACHABILITY ANALYSIS ---------------------------------------------------

% reachability analysis for \mu = 1
options1.R0 = R01;
options1.tFinal = 7;
tic
HA1 = reach(HA1,options1);
tComp = toc;
disp(['computation time for van der Pol (\mu = 1): ',num2str(tComp)]);

% reachability analysis for \mu = 2
options2.R0 = R02;
options2.tFinal = 8;
tic
HA2 = reach(HA2,options2);
tComp = toc;
disp(['computation time for van der Pol (\mu = 2): ',num2str(tComp)]);





% VISUALIZATION -----------------------------------------------------------

% get reachable set 
Rcont1 = get(HA1,'continuousReachableSet');
Rcont2 = get(HA2,'continuousReachableSet');

figure 
hold on

% plot reachable set (\mu = 1)
redOrder = 4;
Rplot = Rcont1.OT;
for iMode = 1:length(Rplot)
    for iSet = 1:length(Rplot{iMode})
        Rtmp = reduce(project(Rplot{iMode}{iSet}{1},[1 2]),'girard',redOrder);
        plotFilled(Rtmp,[1 2],'r','EdgeColor','none'); 
        plot(Rtmp,[1,2],'r');
    end
end

%plotFilled(R01,[1 2],'w','EdgeColor','k'); %plot initial set


% plot reachable set (\mu = 2)
redOrder = 4;
Rplot = Rcont2.OT;
for iMode = 1:length(Rplot)
    for iSet = 1:length(Rplot{iMode})
        Rtmp = reduce(project(Rplot{iMode}{iSet}{1},[1 2]),'girard',redOrder);
        plotFilled(Rtmp,[1 2],'b','EdgeColor','none'); 
        plot(Rtmp,[1,2],'b');
    end
end

%plotFilled(R02,[1 2],'w','EdgeColor','k'); %plot initial set

% %plot simulation results
% for n=1:length(simRes)
%     for j=1:length(simRes{n}.x)
%         %plot results
%         plot(simRes{n}.x{j}(:,1), simRes{n}.x{j}(:,2),'k');
%     end
% end

xlim([-2.5,2.5]);
ylim([-4,4]);
xlabel('x');
ylabel('y');
box on

%------------- END OF CODE --------------