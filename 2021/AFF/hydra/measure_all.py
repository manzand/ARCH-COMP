import os
import string
import re
from subprocess import Popen, PIPE

# tools specified, can generalize loops below
tools = ['hydra']
example = {'hydra': []}

# examples organized with input/output file/string results for parsing runtime results
example['hydra'].append(['Building', 'BLDC01', 'BDS01', 'BLDC01-BDS01',
                         '-d 1/250 -r support_function --setting SupportFunctionSetting --skipplot'])
example['hydra'].append(['Building', 'BLDC01', 'BDS01',  'BLDC01-BDS01',
                         '-d 1/250 -r support_function --setting SupportFunctionSetting --skipplot'])
example['hydra'].append(['Platoon', 'PLAD01', 'BND42', 'PLAD01_BND42',
                         '-d 1/10 -r support_function --setting SupportFunctionSetting --skipplot'])
example['hydra'].append(['Platoon', 'PLAD01', 'BND30', 'PLAD01_BND30',
                         '-d 1/10 -t 20 -r support_function --setting NoBoxReduction --skipplot'])
example['hydra'].append(['Heat', 'HEAT3D', 'HEAT01', 'heat_5',
                         '-d 1/500 -t 40 -r SFN --setting SupportFunctionNewDefault --skipplot'])
example['hydra'].append(['Heat', 'HEAT3D', 'HEAT02', 'heat_10',
                         '-d 1/75 -t 40 -r SFN --setting SupportFunctionNewDefault --skipplot'])


# output result time list
output_time_l = {'hydra': []}
safe = {'hydra': []}

breakout_examples = 0

# iterate over all tools and all examples for each tool
for t in tools:
    for i in range(0, len(example[t])):
        # set up command line call for each tool
        if t == 'hydra':
            breakout_examples = 0
            string_cmd = 'docker run -w "/root/hypro/build" hydra ./bin/hydra -m ../examples/arch21/aff/' + \
                example[t][i][3] + '.model ' + example[t][i][4]
        else:
            print('ERROR: tool not indicated')
            break

        if breakout_examples == 0 or i == 0:
            # call system command via subprocess for pipe redirection
            p = Popen(string_cmd, stdout=PIPE,
                      stderr=PIPE, stdin=PIPE, shell=True)

            output = str(p.communicate())

        # search string in stdout
        if t == 'hydra':
            fstr = 'Verification: '
            sstr = 'The model is safe.'
            r_idx = output.rfind(fstr)
            s_idx = output.rfind(fstr)
            r_idx_time_start = int(r_idx) + len(fstr)
            r_idx_time_end = int(r_idx)+len(fstr)+6

            if s_idx >= 0:
                safe['hydra'].append(1)
            else:
                safe['hydra'].append(0)

        # r_idx non-negative if found
        if r_idx >= 0:
            output_time = output[r_idx_time_start: r_idx_time_end]
            # remove non-numeric characters (except .)
            output_time = re.sub("[^\d\.]", "", output_time)
            output_time_l[t].append(float(output_time))
        else:
            print(
                'WARNING: search string not found in output, check result order for consistency')


for i in range(len(example['hydra'])):
    print('hydra,' + example['hydra'][i][1] + "," + example['hydra']
          [i][2] + "," + str(safe['hydra'][i]) + "," + str(output_time_l['hydra'][i]) + ",")
