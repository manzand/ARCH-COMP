function Hf=hessianTensorInt_SUTRACovid_ext(x,u)



 Hf{1} = interval(sparse(9,9),sparse(9,9));

Hf{1}(3,1) = -x(7);
Hf{1}(4,1) = -x(7);
Hf{1}(7,1) = - x(3) - x(4);
Hf{1}(1,3) = -x(7);
Hf{1}(7,3) = -x(1);
Hf{1}(1,4) = -x(7);
Hf{1}(7,4) = -x(1);
Hf{1}(1,7) = - x(3) - x(4);
Hf{1}(3,7) = -x(1);
Hf{1}(4,7) = -x(1);


 Hf{2} = interval(sparse(9,9),sparse(9,9));

Hf{2}(3,2) = -x(7);
Hf{2}(4,2) = -x(7);
Hf{2}(7,2) = - x(3) - x(4);
Hf{2}(2,3) = -x(7);
Hf{2}(7,3) = -x(2);
Hf{2}(2,4) = -x(7);
Hf{2}(7,4) = -x(2);
Hf{2}(2,7) = - x(3) - x(4);
Hf{2}(3,7) = -x(2);
Hf{2}(4,7) = -x(2);


 Hf{3} = interval(sparse(9,9),sparse(9,9));

Hf{3}(3,1) = x(7);
Hf{3}(4,1) = x(7);
Hf{3}(7,1) = x(3) + x(4);
Hf{3}(1,3) = x(7);
Hf{3}(7,3) = x(1);
Hf{3}(8,3) = -1;
Hf{3}(1,4) = x(7);
Hf{3}(7,4) = x(1);
Hf{3}(1,7) = x(3) + x(4);
Hf{3}(3,7) = x(1);
Hf{3}(4,7) = x(1);
Hf{3}(3,8) = -1;


 Hf{4} = interval(sparse(9,9),sparse(9,9));

Hf{4}(3,2) = x(7);
Hf{4}(4,2) = x(7);
Hf{4}(7,2) = x(3) + x(4);
Hf{4}(2,3) = x(7);
Hf{4}(7,3) = x(2);
Hf{4}(2,4) = x(7);
Hf{4}(7,4) = x(2);
Hf{4}(8,4) = -1;
Hf{4}(2,7) = x(3) + x(4);
Hf{4}(3,7) = x(2);
Hf{4}(4,7) = x(2);
Hf{4}(4,8) = -1;


 Hf{5} = interval(sparse(9,9),sparse(9,9));

Hf{5}(8,3) = 1;
Hf{5}(3,8) = 1;


 Hf{6} = interval(sparse(9,9),sparse(9,9));

Hf{6}(8,4) = 1;
Hf{6}(4,8) = 1;


 Hf{7} = interval(sparse(9,9),sparse(9,9));



 Hf{8} = interval(sparse(9,9),sparse(9,9));

