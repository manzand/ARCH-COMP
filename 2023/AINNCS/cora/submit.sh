#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run --mac-address 02:42:ac:11:00:0a -e MLM_LICENSE_FILE=/home/matlab/Documents/MATLAB/license.lic --name "dummy-submit-container-$1" "dummy-submit-$1" -batch run
docker cp "dummy-submit-container-$1":/home/matlab/Documents/MATLAB/results/ ./results
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"