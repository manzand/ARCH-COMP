FROM ubuntu:18.04

# Benchmarks with Z3 backend

ARG SCALA_VERSION=2.12.8
ARG SBT_VERSION=1.3.7
ARG KYX_VERSION_STRING=494
ARG ARCH_YEAR=arch2021
ARG WOLFRAM_ENGINE_PATH=/usr/local/Wolfram/WolframEngine
ARG WOLFRAM_ENGINE_VERSION=12.3
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ=America/New_York

# Install requirements
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    apt-utils \
    software-properties-common \
    curl \
    avahi-daemon \
    wget \
    unzip \
    zip \
    build-essential \
    openjdk-8-jre-headless \
    openjdk-8-jdk \
    git \
    sshpass \
    sudo \
    locales \
    locales-all \
    ssh \
    vim \
    expect \
    libfontconfig1 \
    libgl1-mesa-glx \
    libasound2 \
    util-linux \
    uuid-dev \
    uuid-runtime \
    && rm -rf /var/lib/apt/lists/*

RUN systemctl enable avahi-daemon

# Install Scala
RUN wget -P /tmp https://www.scala-lang.org/files/archive/scala-${SCALA_VERSION}.deb
WORKDIR /tmp/
RUN dpkg -i scala-${SCALA_VERSION}.deb
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install scala
RUN rm scala-${SCALA_VERSION}.deb

# Install SBT
RUN wget https://scala.jfrog.io/artifactory/debian/sbt-${SBT_VERSION}.deb
RUN dpkg -i sbt-${SBT_VERSION}.deb
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install sbt
RUN rm sbt-${SBT_VERSION}.deb

# Install Wolfram Engine (need for build, but not used in benchmarking since Wolfram Engine 12.3 JLink connection fails for missing dependency uuid_generate)
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
RUN wget https://account.wolfram.com/download/public/wolfram-engine/desktop/LINUX && sudo bash LINUX -- -auto -verbose && rm LINUX

# Create benchmark directory
WORKDIR /root/
RUN mkdir ${ARCH_YEAR}
RUN mkdir ${ARCH_YEAR}/kyx${KYX_VERSION_STRING}

# Pull KeYmaera X
WORKDIR /root/
# avoid caching git clone by adding the latest commit SHA to the container
ADD https://api.github.com/repos/LS-Lab/KeYmaeraX-release/git/refs/heads/master version.json
RUN git clone -b ${ARCH_YEAR} --depth 1 https://github.com/LS-Lab/KeYmaeraX-release.git

# Build KeYmaera X
WORKDIR /root/KeYmaeraX-release/
RUN echo "mathematica.jlink.path=${WOLFRAM_ENGINE_PATH}/${WOLFRAM_ENGINE_VERSION}/SystemFiles/Links/JLink/JLink.jar" > local.properties
RUN sbt clean assembly

# Copy KeYmaera X to benchmark directory
WORKDIR /root/KeYmaeraX-release/
RUN cp keymaerax-webui/target/scala-2.12/KeYmaeraX*.jar /root/${ARCH_YEAR}/keymaerax_${KYX_VERSION_STRING}.jar

# Import benchmark index and script
WORKDIR /root/${ARCH_YEAR}
COPY index${KYX_VERSION_STRING}/ ./index${KYX_VERSION_STRING}/
ADD *.xml ./
ADD runKeYmaeraX${KYX_VERSION_STRING}ScriptedBenchmarks ./
ADD runKeYmaeraX${KYX_VERSION_STRING}Benchmarks ./

# Pull KeYmaera X Projects
WORKDIR /root/
RUN git clone -b ${ARCH_YEAR} --depth 1 https://github.com/LS-Lab/KeYmaeraX-projects.git
RUN cp KeYmaeraX-projects/benchmarks/*.kyx /root/${ARCH_YEAR}/kyx${KYX_VERSION_STRING}/
