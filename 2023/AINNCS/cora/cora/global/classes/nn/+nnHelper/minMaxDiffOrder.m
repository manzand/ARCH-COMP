function L = minMaxDiffOrder(coeffs, l, u, f, der1)
% minMaxDiffOrder - compute the maximum and the minimum difference between the activation
% function and a polynomial fit
%
% Syntax:
%    L = nnHelper.minMaxDiffOrder(coeffs, l, u, f, der1)
%
% Inputs:
%    coeffs - coefficients of polynomial
%    l - lower bound of input domain
%    u - upper bound of input domain
%    f - function handle of activation function
%    der1 - bounds for derivative of activation functions
%
% Outputs:
%    L - interval bounding the lower and upper error
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:        Tobias Ladner
% Written:       28-March-2022
% Last update:   31-August-2022 (adjust tol)
% Last revision: ---

%------------- BEGIN CODE --------------

tol = 0.0001;
minPoints = 10000;

if l == u
    % compute exact result directly
    y = f(l);
    yp = polyval(coeffs, l);
    diff = y-yp;
    L = interval(diff, diff);
    return
end

% calculate bounds for derivative of polynomial

der2 = nnHelper.getDerInterval(coeffs, l, u);
der2 = -der2; % '-' as we calculate f(x) - p(x)

der = supremum(abs(der1-der2));

% determine function bounds by sampling
dx = tol / der;
reqPoints = ceil((u - l)/dx);
numPoints = max(reqPoints, minPoints);
% re-calculate tolerance with number of used points
dx = (u-l)/numPoints;
tol = der * dx;

x = linspace(l, u, numPoints);
x = [l, x, u]; % add l, u in case x is empty (der = 0)
y = f(x) - polyval(coeffs, x);
L = interval(min(y)-tol, max(y)+tol);
end

%------------- END OF CODE --------------
