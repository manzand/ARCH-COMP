function [c]=centerSegment(Obj)
% Purpose:  find center for particulate solution
% Pre:      partition object
% Post:     cell(s) which contain the center of the state space.
% Note:     In the deprecated centerSegment.m, c was a structure with c.ind
%           the output of this (new) function and c.val being the center of 
%           the center cell. c.val can now be found using: 
%           segmentCenter(centerSegment(Obj))
% Last Modified: AP, 31.7.17

%interval hull and middle of discretized state space----
center=sum(Obj.intervals,2)/2;
[c]=intersectingSegments(Obj,center);
%-------------------------------------------------------